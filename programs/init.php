<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */



function storeclient_onDeleteAddon()
{
    $addon = bab_getAddonInfosInstance('storeclient');
    
    $addon->removeAllEventListeners();
    
    $addon->unregisterFunctionality('Ovml/Container/StoreClientAddon');
    $addon->unregisterFunctionality('Ovml/Container/StoreClientAddons');
    $addon->unregisterFunctionality('Ovml/Container/StoreClientAddonRequirements');
    $addon->unregisterFunctionality('Ovml/Container/StoreClientAvailableAddonVersions');
    
	return true;
}



function storeclient_upgrade($version_base, $version_ini)
{
    $addon = bab_getAddonInfosInstance('storeclient');

	$addon->addEventListener(
	   'bab_eventBeforeSiteMapCreated',
	   'storeclient_onBeforeSiteMapCreated',
	   'events.php'
	);
	$addon->addEventListener(
	   'LibTimer_eventDaily',
	   'storeclient_onDaily',
	   'events.php'
	);
	
	if (!$version_base) {
    	$addon->addEventListener(
    	    'bab_eventPageRefreshed', 
    	    'storeclient_firstInstall', 
    	    'events.php'
    	);
	}
	
	
	$registry = bab_getRegistryInstance();
	
	// The keys in this folder are also used by ovidentia core
	$registry->changeDirectory('/bab/install_repository/');
	
	if (null === $registry->getValue('list_url')) {
		$registry->setKeyValue('list_url', 'http://store.ovidentia.org');
		$registry->setKeyValue('root_url', 'http://store.ovidentia.org');
		$registry->setKeyValue('autoupgrade', false);
	}
	
	$addon->registerFunctionality('Ovml/Container/StoreClientAddon', 'ovml.class.php');
	$addon->registerFunctionality('Ovml/Container/StoreClientAddons', 'ovml.class.php');
	$addon->registerFunctionality('Ovml/Container/StoreClientAddonRequirements', 'ovml.class.php');
	$addon->registerFunctionality('Ovml/Container/StoreClientAvailableAddonVersions', 'ovml.class.php');
	
	return true;
}
