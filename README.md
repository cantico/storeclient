## Installer des modules depuis un serveur ##

Permet la mise à jour des modules et du noyau d'ovidentia par téléchargement depuis http://store.ovidentia.org

Ce module ajoute un nouveau lien rendu accessible dans le menu administration > ajouter/supprimer des programmes et un bouton "chercher des mises à jour" sur la description de chaque module.